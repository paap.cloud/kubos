# Проект разворачивания Kubernetes кластера на произвольном числе машин

Установка будет происходить в несколько шагов:
- установка VPN, в первую очередь, чтобы решить проблему с кучей портов, которые нужно пробрасывать через NAT
- установка самого кубера

## Установка VPN

Статьи, которые берутся за основу:
- https://itnext.io/setup-privacy-with-openvpn-using-ansible-b9e613a66f85
- https://www.dmosk.ru/miniinstruktions.php?mini=openvpn-easyrsa3
- https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-debian-10

Подглядеть серверные конфиги можно тут
- https://github.com/adcreare/openvpn/blob/master/tls-server-template.conf

Проверить сроки, на которые выпущен сертификат
- https://www.sslshopper.com/certificate-decoder.html

Установка DNS сервера
- https://www.digitalocean.com/community/tutorials/how-to-configure-bind-as-a-caching-or-forwarding-dns-server-on-ubuntu-14-04
- https://mangolassi.it/topic/12877/set-up-bind-server-with-ansible
- https://www.digitalocean.com/community/tutorials/how-to-configure-bind-as-a-private-network-dns-server-on-ubuntu-18-04-ru
- https://unix.stackexchange.com/questions/473062/not-all-nameservers-are-looked-up/473094#473094

## Установка кубера

За основу взята информация из этой публикации
https://www.digitalocean.com/community/tutorials/how-to-create-a-kubernetes-cluster-using-kubeadm-on-ubuntu-16-04-ru

### Полезная доп.информация

Свежую версию flannel нужно качать с https://github.com/flannel-io/flannel
Там будет ссылка https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml
Я заранее скачал и положил этот файл в данный репо

Порты, которые нужно промаппить на нодах (скорее всего, это будет не нужно благодаря OpenVPN)
https://kubernetes.io/docs/reference/ports-and-protocols/#node

Посмотреть доступные версии приложения <app>
apt-cache madison <app>

Посмотреть адрес шлюза
ip route show