#!/bin/bash -xe

apt update;
apt install -y sshpass ansible openvpn dnsmasq ipcalc fping dnsutils jq; # sshpass нужен ансиблу, чтобы работать не по ssh-ключу, а по логину и паролю

# подготовка файла инвентаризации хостов для установки общих зависимостей
{
  echo "[all]";
  echo "master ansible_host=host.docker.internal ansible_port=${MASTER_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
  echo "worker1 ansible_host=host.docker.internal ansible_port=${WORKER1_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
  echo "worker2 ansible_host=host.docker.internal ansible_port=${WORKER2_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
  echo "worker3 ansible_host=host.docker.internal ansible_port=${WORKER3_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
  echo "worker4 ansible_host=host.docker.internal ansible_port=${WORKER4_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
} > hosts;

# установка общих зависимостей
ansible-playbook -i hosts common-dependencies.yml;

# подготовка файла инвентаризации хостов для настройки vpn
{
  echo "[servers]";
  echo "master ansible_host=host.docker.internal ansible_port=${MASTER_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
  echo "";
  echo "[clients]";
  echo "worker1 ansible_host=host.docker.internal ansible_port=${WORKER1_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
  echo "worker2 ansible_host=host.docker.internal ansible_port=${WORKER2_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
  echo "worker3 ansible_host=host.docker.internal ansible_port=${WORKER3_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
  echo "worker4 ansible_host=host.docker.internal ansible_port=${WORKER4_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
} > openvpn/hosts;

# установка openvpn
VIRTUAL_NETWORK_ADDRESS=$(ipcalc --nobinary "$VIRTUAL_NETWORK_CIDR" | grep Address: | sed 's/^Address:\s\+\([0-9.]\+\)\s*$/\1/');
VIRTUAL_NETWORK_NETMASK=$(ipcalc --nobinary "$VIRTUAL_NETWORK_CIDR" | grep Netmask: | sed 's/^Netmask:\s\+\([0-9.]\+\) = .*/\1/');
VIRTUAL_NETWORK_GATEWAY=$(ipcalc --nobinary "$VIRTUAL_NETWORK_CIDR" | grep HostMin: | sed 's/^HostMin:\s\+\([0-9.]\+\)\s*$/\1/');
ansible-playbook -i openvpn/hosts openvpn/playbook.yml \
  --extra-vars "master_ip=$MASTER_IP dns_ip=$VIRTUAL_NETWORK_GATEWAY" \
  --extra-vars "virtual_network_cidr=$VIRTUAL_NETWORK_CIDR virtual_network_address=$VIRTUAL_NETWORK_ADDRESS virtual_network_netmask=$VIRTUAL_NETWORK_NETMASK";

# подключение докера в созданную openvpn сеть
## Copy common certs and keys from server
cp openvpn/share/server/* /etc/openvpn/client/;
## Copy docker cert and key from server
cp openvpn/share/clients/docker.key /etc/openvpn/client/;
cp openvpn/share/clients/docker.crt /etc/openvpn/client/;
## Generate client configuration from sample config
cd /etc/openvpn/client/;
cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf docker.conf;
## Adjust OpenVPN client configuration
sed -i "s/^remote my-server-1 1194$/remote host.docker.internal 1194/g" docker.conf;
sed -i "s/^;user nobody$/user nobody/g" docker.conf;
sed -i "s/^;group nogroup$/group nogroup/g" docker.conf;
sed -i "s/^cert .*$/cert docker.crt/g" docker.conf;
sed -i "s/^key .*$/key docker.key/g" docker.conf;
sed -i "s/^proto .*$/proto tcp4/g" docker.conf;
echo "" >> docker.conf;
echo "log /var/log/openvpn/openvpn.log" >> docker.conf;

# В докере файл /etc/resolv.conf невозможно удалить, т.к. он заблокирован, поэтому создать
# на его месте символьную ссылку тоже нельзя, что и пытается сделать openvpn командами ниже
#echo "" >> docker.conf;
#echo "script-security 2" >> docker.conf;
#echo "up /etc/openvpn/update-resolv-conf" >> docker.conf;
#echo "down /etc/openvpn/update-resolv-conf" >> docker.conf;

# Но после запуска контейнера поменять содержимое /etc/resolv.conf все же можно
echo "nameserver 127.0.0.1" > /etc/resolv.conf;
# настроив поиск имен в локальном dns (dnsmasq)
{
  echo "no-resolv";
  echo "server=/host.docker.internal/127.0.0.11"; # 127.0.0.11 - внутренний докерный DNS
  echo "server=$VIRTUAL_NETWORK_GATEWAY";
} > /etc/dnsmasq.conf;
dnsmasq;

## Start openvpn systemd service
# TODO удостовериться, что docker всегду будет получать один и тот же IP, сколько бы раз и в какое время его бы ни запускали
openvpn --config docker.conf &

# проверить, что openvpn настроен правильно
cd ~/ansible;
VIRTUAL_NETWORK_SIZE_EXPECTED=$(expr "$(cat openvpn/hosts | grep -c ^\\w)" + 1); # + 1 - это учет самого докера, который так же является клиентом OpenVPN
# FIXME Вместо ${VIRTUAL_NETWORK_ADDRESS}/24 надо использовать $VIRTUAL_NETWORK_CIDR, однако для сети 16 - это слишком долго
VIRTUAL_NETWORK_SIZE_ACTUAL=$(fping -ag "${VIRTUAL_NETWORK_ADDRESS}"/24 | wc -l);
if [ "$VIRTUAL_NETWORK_SIZE_ACTUAL" != "$VIRTUAL_NETWORK_SIZE_EXPECTED" ]; then
  echo "ERROR: virtual network size equals $VIRTUAL_NETWORK_SIZE_ACTUAL but must be $VIRTUAL_NETWORK_SIZE_EXPECTED.";
  exit 1;
fi
echo "OpenVPN - OK";

# подготовка файла инвентаризации хостов для настройки DNS
{
  echo "[servers]";
  echo "master ansible_host=host.docker.internal ansible_port=${MASTER_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
  echo "";
  echo "[clients]";
  echo "worker1 ansible_host=host.docker.internal ansible_port=${WORKER1_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
  echo "worker2 ansible_host=host.docker.internal ansible_port=${WORKER2_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
  echo "worker3 ansible_host=host.docker.internal ansible_port=${WORKER3_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
  echo "worker4 ansible_host=host.docker.internal ansible_port=${WORKER4_PORT} ansible_user=${USER_NAME} ansible_password=${USER_PASSWORD} ansible_sudo_pass=${USER_PASSWORD}";
} > dns/hosts;

# подготовка реестра какому хосту какой IP был выдан при подключении к сети OpenVPN
VPN_IP=$(ip route | grep tun0 | grep -v via | sed 's/.* src \([0-9.]*\).*/\1/');
REVERSE_VPN_IP=$(echo $VPN_IP | awk -F . '{print $4"."$3"."$2"."$1}');
echo "{ \"vpn_hosts\": [ { \"name\": \"docker\", \"ip\": \"$VPN_IP\", \"reverse_ip\": \"$REVERSE_VPN_IP\" } ] }" > openvpn/inventory/docker.json;

# https://e.printstacktrace.blog/merging-json-files-recursively-in-the-command-line/
jq -s '
      def deepmerge(a;b):
          reduce b[] as $item (a;
              reduce ($item | keys_unsorted[]) as $key (.;
                  $item[$key] as $val | ($val | type) as $type | .[$key] = if ($type == "object") then
                      deepmerge({}; [if .[$key] == null then {} else .[$key] end, $val])
                  elif ($type == "array") then
                      (.[$key] + $val)
                  else
                      $val
                  end
              )
          );
      deepmerge({}; .)' openvpn/inventory/* > inventories.json;

echo "Show inventories registry";
cat openvpn/inventory/*;
cat inventories.json;

# установка DNS
ansible-playbook -i dns/hosts dns/playbook.yml \
  --extra-vars "dns_ip=$VIRTUAL_NETWORK_GATEWAY" \
  --extra-vars "@inventories.json";

# проверить, что DNS настроен правильно
if [ $(nslookup google.com | grep -c "Address:\s\+127.0.0.1#53") != 1 ]; then
  echo "Using incorrect DNS server";
  exit 1;
fi
if [ $(nslookup google.com | grep -c "** server can't find") != 0 ]; then
  echo "DNS not working";
  exit 1;
fi
for (( index=0; index<$(jq -s '.[0].vpn_hosts' inventories.json | jq length); index++ )); do
  name=$(jq -sr ".[0].vpn_hosts[$index].name" inventories.json);
  ip=$(jq -sr ".[0].vpn_hosts[$index].ip" inventories.json);
  reverse_ip=$(jq -sr ".[0].vpn_hosts[$index].reverse_ip" inventories.json);
  echo "Check DNS for name=$name ip=$ip reverse_ip=$reverse_ip";
  if [ $(nslookup $name | grep -c "Address: $ip") != 1 ]; then
    echo "DNS server's zones configured incorrectly";
    exit 1;
  fi
  if [ $(nslookup $ip | grep -c "$reverse_ip.in-addr.arpa\s\+name = $name.") != 1 ]; then
    echo "DNS server's reverse zones configured incorrectly";
    exit 1;
  fi
done
echo "DNS - OK";

# Позволяем контейнеру не останавливаться
tail -f /dev/null;